(define-module (thread-example conditions)
  #:use-module ((ice-9 threads) :prefix t:)
  #:use-module ((ice-9 q) :prefix q:)
  #:use-module ((ice-9 match)))

;; IPC between threads using queues, mutexes and condition variables
;; directly with no abstracting datastructure.

(define *main-queue* (q:make-q))
(define *main-cond* (t:make-condition-variable))
(define *client-queue* (q:make-q))
(define *client-cond* (t:make-condition-variable))

(define *client-mutex* (t:make-mutex))
(define *main-mutex* (t:make-mutex))

(define %run (fluid->parameter (make-thread-local-fluid #t)))

(define (client-rec)
  (when (q:q-empty? *client-queue*)
      (t:wait-condition-variable *client-cond* *client-mutex*))
  (catch 'q-empty
    (λ ()
      (let* ((msg (q:q-pop! *client-queue*)))
        (match msg
          ('ping (begin
                   (t:with-mutex *main-mutex*
                                 (q:q-push! *main-queue* 'pong))
                   (t:signal-condition-variable *main-cond*)))
          ('bye (begin
                  (t:with-mutex *main-mutex*
                                (q:q-push! *main-queue* 'cya))
                  (t:signal-condition-variable *main-cond*)
                  (%run #f)))
          (_ #f))))
    (λ (ct) #f)))

(define (client)
  (while (%run)
    (t:with-mutex *client-mutex* (client-rec))))

(define (main)
  (t:call-with-new-thread client)
  (t:with-mutex *client-mutex*
                (q:q-push! *client-queue* 'ping))
  (t:signal-condition-variable *client-cond*)
  (display "Sent ping, received...")
  (newline)
  (t:with-mutex *main-mutex*
                (when (q:q-empty? *main-queue*)
                    (t:wait-condition-variable *main-cond* *main-mutex*))
                (display (q:q-pop! *main-queue*)))
  (newline)
  (t:with-mutex *client-mutex*
                (q:q-push! *client-queue* 'bye))

  (t:signal-condition-variable *client-cond*)
  (display "Sent bye, received...")
  (newline)
  (t:with-mutex *main-mutex*
                (when (q:q-empty? *main-queue*)
                  (t:wait-condition-variable *main-cond* *main-mutex*))
                (display (q:q-pop! *main-queue*)))
  (newline))
