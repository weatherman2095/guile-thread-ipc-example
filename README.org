* Thread IPC examples

** How to run the examples
First load the files into your [[https://www.gnu.org/software/guile/][Guile]] REPL.
#+begin_src bash
  guile -l conditions.scm -l messages.scm
#+end_src

Then enter the following two expressions as you want.

#+begin_src scheme
  ((@@ (thread-example conditions) main))
  ((@@ (thread-example msg) main))
#+end_src

** Why this?

Some half-remembered post on HackerNews against [[https://en.wikipedia.org/wiki/Message_queue][message queues]] as
overcomplicated abstractions that lead to harder-to-maintain code,
with [[https://en.wikipedia.org/wiki/Remote_procedure_call][RPC]] being always preferable.

I decided to first show that messages queues are simpler than doing
without similar abstractions, for example [[file:messages.scm][(thread-example msg)]] has
about half as many lines as the [[file:conditions.scm][(thread-example conditions)]] and is far
more straight-forward to read.

There's also no reason that one cannot implement synchronous RPC with
message queues, which is in fact explicitly supported in [[https://www.erlang.org/doc/man/gen_server.html#call-2][Erlang's OTP
library's gen_server behavior module]] (with whole [[https://en.wikipedia.org/wiki/Erlang_(programming_language)#Concurrency_and_distribution_orientation][language being built
around message passing & message queues]]). It also obviously supports
[[https://www.erlang.org/doc/man/gen_server.html#send_request-2][asynchronous RPC interfaces]] as well.

So pitting message queues vs RPC is nonsensical to start with, as they
represent different abstraction levels where the second can be
implemented on top of various transports including but not limited to
the first anyway (you could also reasonably argue that all transports
amount to message passing and all of them which implement any
buffering are /very similar/ to message queues).

The actual code implemented by [[https://git.savannah.nongnu.org/cgit/guile-lib.git/tree/src/container/async-queue.scm][(container async-queue)]] isn't all that
complicated either. It's about 32 lines.
