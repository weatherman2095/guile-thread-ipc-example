(define-module (thread-example msg)
  #:use-module ((ice-9 threads) :prefix t:)
  #:use-module ((container async-queue) :prefix aq:)
  #:use-module ((ice-9 match)))

;; IPC between threads using message queues

;; This does essentially the same thing as the other file. One seems a
;; lot easier to read & write.

(define *client-inbox* (aq:make-async-queue))
(define *main-inbox* (aq:make-async-queue))

(define (client)
  (let ((run #t))
    (while run
      (match (aq:async-dequeue! *client-inbox*)
        ('ping (aq:async-enqueue! *main-inbox* 'pong))
        ('bye (begin
                (aq:async-enqueue! *main-inbox* 'cya)
                (set! run #f)))
        (_ #f)))))

(define (main)
  (t:call-with-new-thread client)
  (display "Sending ping")
  (newline)
  (aq:async-enqueue! *client-inbox* 'ping)
  (display "Received... ")
  (display (aq:async-dequeue! *main-inbox*))
  (newline)
  (display "Sending bye")
  (newline)
  (aq:async-enqueue! *client-inbox* 'bye)
  (display "Received... ")
  (display (aq:async-dequeue! *main-inbox*))
  (newline))
